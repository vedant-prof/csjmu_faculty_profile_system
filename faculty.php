<!DOCTYPE html>
<html>

<head>
    <title>UIET Faculty System</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type='text/javascript' src="js/angular.min.js">
    </script>
    <script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script type='text/javascript'  src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
    </script>
        
<script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

    <script type='text/javascript' src="script/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css" />
     
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>

    .nav-heading{font-family: "Comic Sans MS", cursive, sans-serif;font-weight: bold;}
 #profile {
    border-radius: 50%;
    text-align: center;
    margin: 0 auto;
    float: right;
    }

    #id01{//width: 69%;
        margin: 0 auto;padding: 0px;border-radius: 29px;overflow-y:scroll;}
    table{
        width:99%;
        padding: 5px;
        text-align: center;
        margin: 0 auto;
    }
    table td{
        padding:5px;
        text-align: left;
        margin-top:7px;
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 600;
        font-size: 17px;
        opacity: 0.85;
    } 
    table th{
        padding:5px;
        font-size: 18px;
        color:rgb(139,0,0);
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 600;
    }
    .disp_new_line{display: block;}
    .modal-content span i{font-size: 22px;}
    .bot-modal{color: red;font-size: 13px;text-align: center;}
    .center{margin-top: 30px;margin-bottom: 160px;}
    .in-center{text-align:center;}
    .in-center-one{//border-right: 1px solid rgb(139,0,0);}
    button{}
    label{color:rgb(139,0,0);}
     footer{width:100%;background:rgb(139,0,0);text-align: center;font-size: 17px;position: relative;top:55px;left: 0;}
     @media screen and (max-width: 992px) {
        .center{margin-top: 30px;margin-bottom: 100px;}
        .in-center-one{margin-bottom: 60px;}
     }
    </style>

</head>

<body>

    <div class="w3-container w3-center " style="background:rgb(139,0,0);">
        <img src="img/logo.png" class="w3-circle w3-hover-blue" style="width: 100px">
        <h3 class="w3-padding w3-text-white nav-heading"> UNIVERSITY INSTITUTE OF ENGINEERING AND TECHNOLOGY<br>CSJM University, KANPUR</h3>
    </div>
 <a href="index.php" style="color:rgb(139,0,0);;text-decoration:underline;font-size:16px;font-weight:700;"> Back to previous page </a>
    <div class="container center">
            <table class="table table-striped table-hover">
                <tr>
                <th> Serial number </th>
                <th> Faculty name </th>
                <th> Department </th>
                <th> Details</th>
                </tr>
<?php
                include('database_connection.php');
                $show="SELECT * from register";
                $result=mysqli_query($conn,$show);
                $i=1;
                while($rows=mysqli_fetch_array($result))
                {
                    $name = $rows['name'];
                    $appointment = $rows['academicdep'];
                    $uid = $rows['id'];
                    $serial_num = $i++;
                ?>
                <tr>
                <td><?=$serial_num?></td>
                <td><?=$name?></td>
                <td><?=$appointment?></td>
                <td><a href='faculty2.php?id=<?=$uid?>' style="color:rgb(139,0,0);text-decoration:underline;">Show all</td>
                </tr>
                <?php
                } ?>

            </table>
    </div>

    <footer class="w3-container w3-padding w3-text-white nav-heading" style=""
    <p class="w3-center w3-large">&copy All Rights Reserved 2018.UIET CSJM University Kanpur | <a href="developer.html" class="w3-button w3-round" style="text-decoration: none;">Developer</a></p>
</footer>


  </body>
  <script>
  document.getElementById('label-one').style.display = 'none';
  document.getElementById('label-two').style.display = 'none';
$(document).ready(function(){
 
    $("#label-one").show('50');
  
});

$(document).ready(function(){
    $("#label-two").show('50');
 
  });


</script>
  </html>