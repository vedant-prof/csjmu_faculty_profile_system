<?php
//index.php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>UIET Faculty System</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type='text/javascript' src="js/angular.min.js">
    </script>
    <script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script type='text/javascript'  src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
    </script>
        
<script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

    <script type='text/javascript' src="script/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css" />
     
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>

    .nav-heading{font-family: "Comic Sans MS", cursive, sans-serif;font-weight: bold;}
 #profile {
    border-radius: 50%;
    text-align: center;
    margin: 0 auto;
    float: right;
    }

    #id01{//width: 69%;
        margin: 0 auto;padding: 0px;border-radius: 29px;overflow-y:scroll;}
    .table-modal-disp{
        width:93%;
        padding: 5px;
        //float: right;
        text-align: center;
        margin: 0 auto;
    }
    .table-modal-disp td{
        width:70%;
        text-align: left;
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 600;
        font-size: 17px;
        opacity: 0.85;
    } 
    .table-modal-disp th{
        width:30%;
        font-size: 16px;
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 650;
    }
    .disp_new_line{display: block;}
    .modal-content span i{font-size: 22px;}
    .bot-modal{color: red;font-size: 13px;text-align: center;}
    label{color:rgb(139,0,0);}
     footer{width:100%;background:rgb(139,0,0);text-align: center;font-size: 17px;}
    </style>

    </style>

</head>

<body>

    <div class="w3-container w3-center " style="background:rgb(139,0,0);">
        <img src="img/logo.png" class="w3-circle w3-hover-blue" style="width: 100px">
        <h3 class="w3-padding w3-text-white nav-heading"> UNIVERSITY INSTITUTE OF ENGINEERING AND TECHNOLOGY<br>CSJM University, KANPUR</h3>
    </div>


    <div ng-app="login_register_app" ng-controller="login_register_controller" class="w3-container w3-padding-32">
        <?php
   if(!isset($_SESSION["name"]))
   {
   ?>
        <div class="alert {{alertClass}} alert-dismissible" ng-show="alertMsg">
            <a href="#" class="close" ng-click="closeMsg()" aria-label="close">&times;</a>
            {{alertMessage}}
        </div>

        <br><br>

        <div class="panel panel-default" ng-show="login_form">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <form method="post" ng-submit="submitLogin()">
                    <div class="form-group">
                        <label>Enter Your UserId</label>
                        <input type="text" name="userid" ng-model="loginData.userid" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Enter Your Password</label>
                        <input type="password" name="password" ng-model="loginData.password" class="form-control" required/><br>
                        <a href="index.php" style="color:rgb(139,0,0);;text-decoration:underline;font-size:17px;font-weight:700;"> Back to previous page </a>
                    </div>
                    <div class="form-group" align="center">

                        <button type="submit" name="login" class="btn btn-danger" value="Login" style='width:20%;border-radius:5px;padding:10px;'>Login</button>
                        <br />
                   <!--    <input type="button" name="register_link" class="btn btn-primary btn-link" ng-click="showRegister()" value="Register" />-->
                    </div>
                </form>
            </div>
        </div>
    <!--    <div class="panel panel-default" ng-show="register_form">
            <div class="panel-heading">
                <h3 class="panel-title">Register</h3>
            </div>
            <div class="panel-body">
               <form method="post" ng-submit="submitRegister()" >
               <!--     <div class="form-group">
                        <label>Enter Your Name</label>
                        <input type="text" name="name" ng-model="registerData.name" class="form-control" />
                    </div>-->
        <!--        <div class="form-group">
                        <label>Enter User Id</label>
                        <input type="text" name="userid" ng-model="registerData.userid" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Enter Your Password</label>
                        <input type="password" name="password" ng-model="registerData.password" class="form-control" />
                    </div>
                    <div class="form-group" align="center">
                        <input type="submit" name="register" class="btn btn-primary" value="Register" />
                        <br />
                        <input type="button" name="login_link" class="btn btn-primary btn-link" ng-click="showLogin()" value="Login" />
                    </div>
                </form>
            </div>
        </div>-->
        
        <?php
   }
   else
   {
   ?>
        <div class="w3-container w3-padding">

            <div class="row">
    <div class="col-xs-7">
      <div class="thumbnail">
        <?php
               $uploads_dir = 'faculty_data/';

               $imagename = $uploads_dir.$_SESSION['name'] . '.png';

           if(file_exists("$imagename"))
           {
           echo " <img id='profile' width='210px' height=' 210px' src='".$imagename."'>";
       } 
       else
            {
                echo " <img  id='profile' width='200px' height=' 230px' src='no_image.png'>";
            }

       ?>
          
          <div class="caption">
            <p><span class="w3-wide nav-heading " style="font-size:17px;font-weight:700;border-radius:5px;color:rgb(139,0,0);">
                <?php echo strtoupper($_SESSION["name"]);
                $uid=$_SESSION["name"];
                ?>
             </span> 
          </p>
          </div>
      </div>
    </div>
<?php
                include('database_connection.php');
                $show="SELECT*from register WHERE userid='$uid'";
                $result=mysqli_query($conn,$show);
                while($rows=mysqli_fetch_array($result))
                {
                    $name=$rows['name'];
                    $appointment=$rows['appointment'];
                    $addappointment=$rows['addappointment'];
                    $academicdep=$rows['academicdep'];
                    $address1=$rows['address1'];
                    $address2=$rows['address2'];
                    $address3=$rows['address3'];
                    $address4=$rows['address4'];
                    $email=$rows['emails'];
                    $phone=$rows['phone'];
                    $award1=$rows['award1'];
                    $award2=$rows['award2'];
                    $award3=$rows['award3'];
                    $award4=$rows['award4'];
                    $award5=$rows['award5'];
                    $qualification1=$rows['qualification1'];
                    $qualification2=$rows['qualification2'];
                    $qualification3=$rows['qualification3'];
                    $qualification4=$rows['qualification4'];
                    $qualification5=$rows['qualification5'];
                    $research1=$rows['research1'];
                    $research2=$rows['research2'];
                    $research3=$rows['research3'];
                    $interest=$rows['interest'];
                    $membership=$rows['membership'];
                    $publication1=$rows['publication1'];

                    $publication2=$rows['publication2'];

                    $publication3=$rows['publication3'];

                    $publication4=$rows['publication4'];

                    $publication5=$rows['publication5'];
                    $ongoing1=$rows['ongoing1'];
                    $ongoing2=$rows['ongoing2'];
                    $ongoing3=$rows['ongoing3'];

                    $completed1=$rows['completed1'];
                    $completed2=$rows['completed2'];
                    $completed3=$rows['completed3'];

                    $news1=$rows['news1'];
                    $news2=$rows['news2'];
                    $news3=$rows['news3'];
                    

                    $status=$rows["status"];
                }

            ?>
    
               
            
            <div class="row">
    <div class="col-xs-5">
       
         
                <div class="w3-right " style="color:rgb(139,0,0);">
                    <b>
                    Status :
                </b>  
            <?php
            if($status==0)
            {
                echo "<span class='w3-text-red '>Pending</span>";
            }
            else
                {
                    echo "<span class='w3-text-green'>Previously Updated </span>";
                }
                ?>
                <br>
                <br>
                <a class="w3-mobile btn btn-danger w3-right" href="logout.php">Logout</a>
            </div>
          
      </div>
    </div>
            
              
                
            
<!--         <div class="w3-container w3-center">
            <p class="w3-text-green">
    Please Click on the "Preview Form" button to view how your given details looks like in the form.
</p>
<button onclick="document.getElementById('id01').style.display='block'" class="w3-margin-bottom btn btn-info w3-round">Preview Form</button>

        </div>-->
        </div>


        <div class="container" style="width:750px;">
            <br /><br />
            <div ng-controller="formcontroller">
              
                    <p class="text-center" style="color:rgb(139,0,0);">
                        Fields with '*' are mandatory.
                    </p>
                <form name="userForm" ng-submit="insertData()">
                    <label class="text-success" ng-show="successInsert">{{successInsert}}</label>
                    <div class="form-group">
                        <label>Full Name<span class="text-danger">*</span></label>
                        <input id="full_name_s" type="text" name="name" ng-model="insert.name" ng-init="insert.name='<?php echo $name;?>'" class="form-control" required/>

                    </div>
                    <div class="form-group">
                        <label>Academic Appointment<span class="text-danger">*</span>
                        </label>

                        (Such as Assistant Professor/ Associate Professor/ Professor etc)
                        <input id="Academic_Appointment_s" type="text" name="last_name" ng-model="insert.appointment" ng-init="insert.appointment='<?php echo $appointment;?>'" class="form-control" required/>

                    </div>
                    <br />
                    <div class="form-group">
                        <label>Additional Appointment</label>
                        (Such as Head of the Department, Director etc)
                        <input id="Additional_Appointment_s" type="text" name="first_name" ng-model="insert.addappointment" ng-init="insert.addappointment='<?php echo $addappointment;?>'" class="form-control" />

                    </div>
                    <br/>
                        <div class="form-group">
                            <label>Academic Department<span class="text-danger">*</span></label>
                            (Please enter the ful name of Department/Institute)
                            <input id="Academic_Department_s" type="text" name="last_name" ng-model="insert.academicdep" ng-init="insert.academicdep='<?php echo $academicdep;?>'" class="form-control" required/>

                        </div>
                        <br />
                        <div class="form-group">
                            <label>Contact Address<span class="text-danger">*</span></label>
                            <br>
                            Line 1
                            <input id="Contact_Address_s1" type="text" name="last_name" ng-model="insert.address1" ng-init="insert.address1='<?php echo $address1;?>'" class="form-control" required/>
                            Line 2
                            <input id="Contact_Address_s2" type="text" name="last_name" ng-model="insert.address2" ng-init="insert.address2='<?php echo $address2;?>'" class="form-control" />
                            Line 3
                            <input id="Contact_Address_s3" type="text" name="last_name" ng-model="insert.address3" ng-init="insert.address3='<?php echo $address3;?>'" class="form-control" />
                            Line 4 
                            <input id="Contact_Address_s4" type="text" name="last_name" ng-model="insert.address4" ng-init="insert.address4='<?php echo $address4;?>'" class="form-control" />

                        </div>
                        <br />
                        <div class="form-group">
                            <label>Email<span class="text-danger">*</span></label>
                            (Ex: xyz@domain.com)
                            <input id="Email_s" type="text" name="last_name" ng-model="insert.email"  ng-init="insert.email='<?php echo $email;?>'" class="form-control" required/>

                        </div>
                        <br />
                        <div class="form-group">
                            <label>Phone<span class="text-danger">*</span></label>
                            (Only 10 digits)
                            <input id="Phone_s" type="text" name="last_name" ng-model="insert.phone" ng-init="insert.phone='<?php echo $phone;?>'" class="form-control" required/>

                        </div>
                        <br />
                        <div class="form-group">
                            <label>Awards and Honours</label>
                            (Each one in new line)
                            <input id="Awards_e1" type="text" name="last_name" ng-model="insert.awards1" ng-init="insert.awards1='<?php echo $award1;?>'" class="form-control" />
                            <input id="Awards_e2" type="text" name="last_name" ng-model="insert.awards2" ng-init="insert.awards2='<?php echo $award2;?>'" class="form-control" />
                            <input id="Awards_e3" type="text" name="last_name" ng-model="insert.awards3" ng-init="insert.awards3='<?php echo $award3;?>'" class="form-control" />
                            <input id="Awards_e4" type="text" name="last_name" ng-model="insert.awards4" ng-init="insert.awards4='<?php echo $award4;?>'" class="form-control" />
                            <input id="Awards_e5" type="text" name="last_name" ng-model="insert.awards5" ng-init="insert.awards5='<?php echo $award5;?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>Educational Qualifications<span class="text-danger">*</span></label>
                            (Each One in new line)
                            <input id="Education_s" type="text" name="last_name" ng-model="insert.qualification1" ng-init="insert.qualification1='<?php echo $qualification1;?>'" class="form-control" required/>
                             <input id="Education_s" type="text" name="last_name" ng-model="insert.qualification2" ng-init="insert.qualification2='<?php echo $qualification2;?>'" class="form-control" required/>
                              <input id="Education_s" type="text" name="last_name" ng-model="insert.qualification3" ng-init="insert.qualification3='<?php echo $qualification3;?>'" class="form-control" />
                               <input id="Education_s" type="text" name="last_name" ng-model="insert.qualification4" ng-init="insert.qualification4='<?php echo $qualification4;?>'" class="form-control" />
                                <input id="Education_s" type="text" name="last_name" ng-model="insert.qualification5" ng-init="insert.qualification5='<?php echo $qualification5;?>'" class="form-control" />


                        </div>
                        <br />

                        <div class="form-group">
                            <label>Research Interests Details<span class="text-danger">*</span></label>
                            (Top three Researches/each in new line)
                            <input id="Research_s" type="text" name="last_name" ng-model="insert.research1" ng-init="insert.research1='<?php echo $research1;?>'" class="form-control" required/>
                            <input id="Research_s" type="text" name="last_name" ng-model="insert.research2" ng-init="insert.research2='<?php echo $research2;?>'" class="form-control" />
                            <input id="Research_s" type="text" name="last_name" ng-model="insert.research3" ng-init="insert.research3='<?php echo $research3;?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>Membership of Professional Bodies</label>
                            (Please fill your elaborated research interest/Optional)
                            <input id="Membership_s" type="text" name="last_name" ng-model="insert.membership" ng-init="insert.membership='<?php echo $membership;?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>Highlighted Publications</label>
                            (Each in a fresh Line)
                            <input id="Highlighted_s1" type="text" name="last_name" ng-model="insert.publication1" ng-init="insert.publication1='<?php echo $publication1;?>'" class="form-control" />
                            <input id="Highlighted_s2" type="text" name="last_name" ng-model="insert.publication2" ng-init="insert.publication2='<?php echo $publication2;?>'" class="form-control" />
                            <input id="Highlighted_s3" type="text" name="last_name" ng-model="insert.publication3" ng-init="insert.publication3='<?php echo $publication3;?>'" class="form-control" />
                            <input id="Highlighted_s4" type="text" name="last_name" ng-model="insert.publication4" ng-init="insert.publication4='<?php echo $publication4;?>'" class="form-control" />
                            <input id="Highlighted_s5" type="text" name="last_name" ng-model="insert.publication5" ng-init="insert.publication5='<?php echo $publication5;?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>Ongoing Projects</label>
                            (Each one in a fresh line,(Please mention Title,Amount,Duration and sponsoring agency )
                            <input id="Ongoing_s1" type="text" name="last_name" ng-model="insert.ongoing1" ng-init="insert.ongoing1='<?php echo $ongoing1;?>'" class="form-control" />
                            <input id="Ongoing_s2" type="text" name="last_name" ng-model="insert.ongoing2" ng-init="insert.ongoing2='<?php echo $ongoing2;?>'" class="form-control" />
                            <input id="Ongoing_s3" type="text" name="last_name" ng-model="insert.ongoing3" ng-init="insert.ongoing3='<?php echo $ongoing3;?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>Completed Projects</label>
                            (Each one in a fresh line,(Please mention Title,Amount,Duration and sponsoring agency )
                            <input id="Completed_s1" type="text" name="last_name" ng-model="insert.completed1" ng-init="insert.completed1='<?php echo $completed1?>'" class="form-control" />
                            <input id="Completed_s2" type="text" name="last_name" ng-model="insert.completed2" ng-init="insert.completed2='<?php echo $completed2?>'" class="form-control" />
                            <input id="Completed_s3" type="text" name="last_name" ng-model="insert.completed3" ng-init="insert.completed3='<?php echo $completed3?>'" class="form-control" />

                        </div>
                        <br />

                        <div class="form-group">
                            <label>In the news</label>
                            (Links to print, broadcast or internet news stories about your works)
                            <input id="News_s1"type="text" name="last_name" ng-model="insert.news1" ng-init="insert.news1='<?php echo $news1;?>'" class="form-control" />
                            <input id="News_s2"type="text" name="last_name" ng-model="insert.news2" ng-init="insert.news2='<?php echo $news2;?>'" class="form-control" />
                            <input id="News_s3"type="text" name="last_name" ng-model="insert.news3" ng-init="insert.news3='<?php echo $news3;?>'" class="form-control" />
                        </div>
                        <br />

<!-- 
</br>
<div class="form-group">
    <input type="file" name="Upload your CV" class="btn btn-info" placeholder="Upload your CV" />
</div>
</br>

-->
<span style="color:red;font-size:12px;color:rgb(139,0,0);font-weight:bold;">* Scroll down to select "Preview Form"</span><div class="form-group w3-center">
    <input type="submit" name="savemydetails" class="btn btn-info" value="Save my details" />  
</div>





                   
</form>


<div style="border-top:1px solid grey"><br>
<label>Upload your CV (pdf)</label>
<form class="w3-padding" id="upload" method="POST" enctype="multipart/form-data" action="fileupload.php">
<div style="width:50%"; class="w3-left">
<input class="w3-button" type="file" name="file" id="file"><br>
</div>

<div style="width:50%"; class="w3-right">
    <button type="submit" class="btn btn-info w3-round w3-center" > Upload CV </button>
</div>
</form>
<br><br>
<progress id="prog" max="100" value="0" style="display:none;"></progress>
<div id="percent"></div>

<div id="here"></div>  <br><br>
</div>

                        <div class="w3-card-4 w3-center" style="border-top:1px solid grey;"><br>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">Select Profile Image
                                <span class="text-primary">*</span></div>
                                <div class="panel-body" align="center">
                                    <input type="file" name="upload_image" id="upload_image" required="" />
                                    <br />
                                    <div id="uploaded_image"></div>
                                </div>
                            </div>
                        </div>

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                            <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'square' //circle
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#upload_image').on('change', function() {
            var reader = new FileReader();
            reader.onload = function(event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event) {
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(response) {
                $.ajax({
                    url: "upload.php",
                    type: "POST",
                    data: {
                        "image": response
                    },
                    success: function(data) {
                        $('#uploadimageModal').modal('hide');
                        $('#uploaded_image').html(data);
                    }
                });
            })
        });

    });
</script>






<div class="w3-container w3-border w3-border-blue w3-round w3-margin-bottom">
    <p class="w3-text-green">
    Please Click on the "Preview Form" button to view how your given details looks like in the form.
</p>
<button onclick="preview()" class="w3-margin-bottom btn btn-info w3-round">Preview Form</button>

</div>

<!-- onclick="document.getElementById('id01').style.display='block'"  ---->




<div id="id01" class="modal">
    <div class="modal-content">
        <div class="w3-container w3-padding">

            <div class="row">
    <div class="col-xs-7">
      <div class="thumbnail">
               <?php   
               $uploads_dir = 'faculty_data/';

               $imagename = $uploads_dir.$_SESSION['name'] . '.png';

           if(file_exists("$imagename"))
           {
           echo " <img id='profile' width='210px' height=' 210px' src='".$imagename."'>";
       } 
       else
            {
                echo " <img  id='profile' width='200px' height=' 230px' src='no_image.png'>";
            }

       ?>
          
          <div class="caption">
            <p><span class="w3-wide nav-heading  w3-text-grey" style="font-size:17px;font-weight:900;border-radius:5px;">
                <?php echo strtoupper($name);
                $uid=$_SESSION["name"];
                ?>
             </span> 
          </p>
          </div>
      </div>
    </div><br><br>
        
            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright"><i class="fa fa-times" aria-hidden="true"></i></span>
            <table class="table table-striped table-hover table-modal-disp">
               
                <tr id="Academic_Appointment_e_show">
                    <th>Academic Appointment :</th>
                    <td id="Academic_Appointment_e">{{insert.appointment}}</td>
                </tr>
                <tr id="Additional_Appointment_e_show">
                    <th>Additional Appointment :</th>
                    <td id="Additional_Appointment_e">{{insert.addappointment}}</td>
                </tr>
                <tr>
                    <th>Academic Department :</th>
                    <td id="Academic_Department_e">{{insert.academicdep}}</td>
                </tr>
                <tr>
                    <th>Contact Address :</th>
                    <td id="Contact_Address_e">{{insert.address1 +" "+insert.address2 +" " + insert.address3 +" "+ insert.address4+" "}}</td>
                </tr>
                <tr>
                    <th>Email :</th>
                    <td id="Email_e">{{insert.email}}</td>
                </tr>
                <tr>
                    <th>Phone :</th>
                    <td id="Phone_e">{{insert.phone}}</td>
                </tr>

                <tr id="Awards_e_show">
                    <th>Awards and Honour :</th>
                    <td>
                        <span class="disp_new_line" id="Awards_ee1">{{insert.awards1}}</span>
                        <span class="disp_new_line"id="Awards_ee2">{{insert.awards2}}</span>
                        <span class="disp_new_line"id="Awards_ee3">{{insert.awards3}}</span>
                        <span class="disp_new_line"id="Awards_ee4">{{insert.awards4}}</span>
                        <span class="disp_new_line"id="Awards_ee5">{{insert.awards5}}</span>
                    </td> 

                     
                </tr>
                <tr>
                    <th>Educational Qualifications :</th>
                    <td id="Education_e_show">
                     <span id="Education_e1" class="disp_new_line">   {{insert.qualification1}} </span>
                     <span id="Education_e2" class="disp_new_line">    {{insert.qualification2}} </span>
                     <span id="Education_e3" class="disp_new_line">   {{insert.qualification3}} </span>
                     <span id="Education_e4" class="disp_new_line">   {{insert.qualification4}} </span>
                     <span id="Education_e5" class="disp_new_line">   {{insert.qualification5}} </span>
                    </td>
                </tr>
                <tr>
                    <th>Research Interests Details :</th>
                    <td id="Research_e"> 
                    <span class="disp_new_line"> {{insert.research1}} </span> 
                    <span class="disp_new_line"> {{insert.research2}} </span> 
                    <span class="disp_new_line"> {{insert.research3}} </span> 
                    </td>
                </tr>
                <tr id="Membership_e_show">
                    <th>Membership of Professional Bodies :</th>
                    <td id="Membership_e">{{insert.membership}}</td>
                </tr>
                <tr id="Highlighted_e_show">
                    <th>Highlighted Publications :</th>
                    <td id="Highlighted_e">
                        <span class="disp_new_line">{{insert.publication1}}</span>
                        <span class="disp_new_line">{{insert.publication2}}</span>
                        <span class="disp_new_line">{{insert.publication3}}</span>
                        <span class="disp_new_line">{{insert.publication4}}</span>
                        <span class="disp_new_line">{{insert.publication5}}</span>
                    </td>
                </tr>
                <tr id="Ongoing_e_show">
                    <th>Ongoing Projects :</th>
                    <td id="Ongoing_e">
                        <span class="disp_new_line">{{insert.ongoing1}}</span>
                        <span class="disp_new_line">{{insert.ongoing2}}</span>
                        <span class="disp_new_line">{{insert.ongoing3}}</span>
                    </td>
                </tr>
                <tr id="completed_e_show">
                    <th>Completed Projects :</th>
                    <td id="completed_e">
                        <span class="disp_new_line">{{insert.completed1}}</span>
                        <span class="disp_new_line">{{insert.completed2}}</span>
                        <span class="disp_new_line">{{insert.completed3}}</span>
                    </td>
                </tr>
                <tr id="News_e_show">
                    <th>In the news :</th>
                    <td id="News_e">
                        <span class="disp_new_line">{{insert.news1}}</span>
                        <span class="disp_new_line">{{insert.news2}}</span>
                        <span class="disp_new_line">{{insert.news3}}</span>
                    </td>
                </tr>
                <?php
                $uploads_dir = 'faculty_data/';
                 $cvname = $uploads_dir.$_SESSION['name'] . '.pdf';
                if(file_exists("$cvname"))
                {   ?>
                <tr>
                <th> Link to CV: </th>
                <td> <a href="<?=$cvname?>" style="color:rgb(139,0,0);text-decoration:underline;"> Click to open your CV </a></td>

                </tr>
                <?php } ?>


            </table> <br>
        
    
<p class=" w3-padding text-center" style="color:rgb(139,0,0);">
    PROCEED !!! Click on "save my details" button if everything seems right.
</p>
    </div>

</div>
</div>
</div>




<?php
   }
   ?>

</div>
 <br><br>

<footer class="w3-container w3-padding w3-text-white nav-heading" style=""
    <p class="w3-center w3-large">&copy All Rights Reserved 2018.UIET CSJM University Kanpur | <a href="developer.html" class="w3-button w3-round" style="text-decoration: none;">Developer</a></p>
</footer>




</body>

</html>

<script>

function preview(){
    document.getElementById('id01').style.display='block';

var ba = document.getElementById('Additional_Appointment_s').value;
var ba = document.getElementById('Additional_Appointment_s').value;

console.log(ba);
if( ba == '')
{
   document.getElementById('Additional_Appointment_e_show').style.display = 'none'; 
}
else
    {console.log("nmcs"); }

var ab1 = document.getElementById('Awards_e1').value;
var ab2 = document.getElementById('Awards_e2').value;
var ab3 = document.getElementById('Awards_e3').value;
var ab4 = document.getElementById('Awards_e4').value;
var ab5 = document.getElementById('Awards_e5').value;
console.log(ab1);console.log(ab2);console.log(ab3);console.log(ab4);console.log(ab5);
if( ab1 == '' && ab2 == '' && ab3 == '' && ab4 == '' && ab5 == '')
   {
    document.getElementById('Awards_e_show').style.visibility = 'hidden';
}
else
    {document.getElementById('Awards_e_show').style.visibility = 'visible';}

var ab1 = document.getElementById('Membership_s').value;
if( ab1 == '')
{
    document.getElementById('Membership_e_show').style.visibility = 'hidden';
}
else
{
    document.getElementById('Membership_e_show').style.visibility = 'visible';
}

var ab1 = document.getElementById('Highlighted_s1').value;
var ab2 = document.getElementById('Highlighted_s2').value;
var ab3 = document.getElementById('Highlighted_s3').value;
var ab4 = document.getElementById('Highlighted_s4').value;
var ab5 = document.getElementById('Highlighted_s5').value;

if( ab1 == '' && ab2 == '' && ab3 == '' && ab4 == '' && ab5 == '')
   {
    document.getElementById('Highlighted_e_show').style.visibility = 'hidden';
}
else
    {document.getElementById('Highlighted_e_show').style.visibility = 'visible';}

var ab1 = document.getElementById('Ongoing_s1').value;
var ab2 = document.getElementById('Ongoing_s2').value;
var ab3 = document.getElementById('Ongoing_s3').value;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('Ongoing_e_show').style.visibility = 'hidden';
}
else
    {document.getElementById('Ongoing_e_show').style.visibility = 'visible';}

var ab1 = document.getElementById('Completed_s1').value;
var ab2 = document.getElementById('Completed_s2').value;
var ab3 = document.getElementById('Completed_s3').value;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('completed_e_show').style.visibility = 'hidden';
}
else
    {document.getElementById('completed_e_show').style.visibility = 'visible';}

var ab1 = document.getElementById('News_s1').value;
var ab2 = document.getElementById('News_s2').value;
var ab3 = document.getElementById('News_s3').value;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('News_e_show').style.visibility = 'hidden';
}
else
    {document.getElementById('News_e_show').style.visibility = 'visible';}
/*
var ab = document.getElementById('Additional_Appointment_s').value;
document.getElementById('Additional_Appointment_e').textContent = ab;

var ab = document.getElementById('Academic_Department_s').value;
document.getElementById('Academic_Department_e').textContent = ab;

var ab1 = document.getElementById('Contact_Address_s1').value; ab1=ab1+",";
var ab2 = document.getElementById('Contact_Address_s2').value; ab2=ab2+",";
var ab3 = document.getElementById('Contact_Address_s3').value; ab3=ab3+" ";
var ab4 = document.getElementById('Contact_Address_s4').value;
document.getElementById('Contact_Address_e').textContent = ab1+ab2+ab3+ab4;

var ab = document.getElementById('Email_s').value;
document.getElementById('Email_e').textContent = ab;

var ab = document.getElementById('Phone_s').value;
document.getElementById('Phone_e').textContent = ab;

var ab1 = document.getElementById('Highlighted_s1').value; ab1=ab1+"|";
var ab2 = document.getElementById('Highlighted_s2').value; ab2=ab2+"|";
var ab3 = document.getElementById('Highlighted_s3').value; ab3=ab3+"|";
var ab4 = document.getElementById('Highlighted_s4').value; ab4=ab4+"|";
var ab5 = document.getElementById('Highlighted_s4').value; 
document.getElementById('Highlighted_e').textContent = ab1+ab2+ab3+ab4+ab5;

var ab = document.getElementById('Education_s').value;
document.getElementById('Education_e').textContent = ab;

var ab = document.getElementById('Awards_s').value;
document.getElementById('Awards_e').textContent = ab;

var ab = document.getElementById('Research_s').value;
document.getElementById('research_e').textContent = ab;

var ab = document.getElementById('Membership_s').value;
document.getElementById('Membership_e').textContent = ab;

var ab = document.getElementById('Ongoing_s').value;
document.getElementById('Ongoing_e').textContent = ab;

var ab = document.getElementById('Completed_s').value;
document.getElementById('Completed_e').textContent = ab;

var ab = document.getElementById('News_s').value;
document.getElementById('News_e').textContent = ab;  */

}

</script>

<script>
    var app = angular.module('login_register_app', []);

    app.controller("formcontroller", function($scope, $http) {
        $scope.insert = {};
        $scope.insertData = function() {
            $http({
                method: "POST",
                url: "insert.php",
                data: $scope.insert,
            }).success(function(data) {
                if (data.error) {
                    $scope.errorname = data.error.first_name;
                    $scope.successInsert = null;
                } else {
                    $scope.insert=null;
                    $scope.errorname = null;
                    
                    $scope.successInsert = data.message;
                    alert("Your data has been inserted successfully");
                   
                }
            });
        }
    });
    app.controller('login_register_controller', function($scope, $http) {
        $scope.closeMsg = function() {
            $scope.alertMsg = false;
        };

        $scope.login_form = true;

        $scope.showRegister = function() {
            $scope.login_form = false;
            $scope.register_form = true;
            $scope.alertMsg = false;
        };

        $scope.showLogin = function() {
            $scope.register_form = false;
            $scope.login_form = true;
            $scope.alertMsg = false;
        };

        $scope.submitRegister = function() {
            $http({
                method: "POST",
                url: "register.php",
                data: $scope.registerData
            }).success(function(data) {
                $scope.alertMsg = true;
                if (data.error != '') {
                    $scope.alertClass = 'alert-danger';
                    $scope.alertMessage = data.error;
                } else {
                    $scope.alertClass = 'alert-success';
                    $scope.alertMessage = data.message;
                    $scope.registerData = {};
                }
            });
        };

        $scope.submitLogin = function() {
            $http({
                method: "POST",
                url: "login.php",
                data: $scope.loginData
            }).success(function(data) {
                if (data.error != '') {
                    $scope.alertMsg = true;
                    $scope.alertClass = 'alert-danger';
                    $scope.alertMessage = data.error;
                    window.location="landing.php";
                } else {
                    //location.reload();
                window.location="landing.php";

                }
            });
        };

    });
</script>
<script>
var main = function()
{
   $("#upload").on('submit',function(e)
                   {
    e.preventDefault();   
       $(this).ajaxSubmit(
       
           {
            beforeSend:function()
               {
                $("#prog").show();
                $("#prog").attr('value','0');
                   
               },
               uploadProgress:function(event,position,total,percentCompelete)
               {
                  $("#prog").attr('value',percentCompelete); 
                   $("#percent").html(percentCompelete+'%');
               },
               success:function(data)
               {
                   $("#here").html(data);
               }
           });
   });
};

$(document).ready(main);

</script>