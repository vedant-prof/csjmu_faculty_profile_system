<?php
//index.php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>UIET Faculty System</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type='text/javascript' src="js/angular.min.js">
    </script>
    <script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script type='text/javascript'  src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
    </script>
        
<script type='text/javascript'  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

    <script type='text/javascript' src="script/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css" />
     
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>

    .nav-heading{font-family: "Comic Sans MS", cursive, sans-serif;font-weight: bold;}
 #profile {
    border-radius: 50%;
    text-align: center;
    margin: 0 auto;
    float: right;
    }

    #id01{//width: 69%;
        margin: 0 auto;padding: 0px;border-radius: 29px;overflow-y:scroll;}
    .table-modal-disp{
        width:93%;
        padding: 5px;
        //float: right;
        text-align: center;
        margin: 0 auto;
    }
    .table-modal-disp td{
        width:70%;
        text-align: left;
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 600;
        font-size: 17px;
        opacity: 0.85;
    } 
    .table-modal-disp th{
        width:30%;
        font-size: 16px;
        font-family: 'Vibur', cursive;
        font-family: 'Source Code Pro', monospace;
        font-weight: 650;
    }
    .disp_new_line{display: block;}
    .modal-content span i{font-size: 22px;}
    .bot-modal{color: red;font-size: 13px;text-align: center;}
    label{color:rgb(139,0,0);}
     footer{width:100%;background:rgb(139,0,0);text-align: center;font-size: 17px;}
    </style>

    </style>

</head>

<body>
    <?php
               $uid = $_GET['id'];
               include('database_connection.php');
               // $conn=mysqli_connect("localhost","root","","techmart");
                $show="SELECT * from `register` WHERE id='$uid'";
                $result=mysqli_query($conn,$show);
                while($rows=mysqli_fetch_array($result))
                {
                   $userid=$rows['userid'];
                    $name=$rows['name'];
                    $appointment=$rows['appointment'];
                    $addappointment=$rows['addappointment'];
                    $academicdep=$rows['academicdep'];
                    $address1=$rows['address1'];
                    $address2=$rows['address2'];
                    $address3=$rows['address3'];
                    $address4=$rows['address4'];
                    $email=$rows['emails'];
                    $phone=$rows['phone'];
                    $award1=$rows['award1'];
                    $award2=$rows['award2'];
                    $award3=$rows['award3'];
                    $award4=$rows['award4'];
                    $award5=$rows['award5'];
                    $qualification1=$rows['qualification1'];
                    $qualification2=$rows['qualification2'];
                    $qualification3=$rows['qualification3'];
                    $qualification4=$rows['qualification4'];
                    $qualification5=$rows['qualification5'];
                    $research1=$rows['research1'];
                    $research2=$rows['research2'];
                    $research3=$rows['research3'];
                    $interest=$rows['interest'];
                    $membership=$rows['membership'];
                    $publication1=$rows['publication1'];

                    $publication2=$rows['publication2'];

                    $publication3=$rows['publication3'];

                    $publication4=$rows['publication4'];

                    $publication5=$rows['publication5'];
                    $ongoing1=$rows['ongoing1'];
                    $ongoing2=$rows['ongoing2'];
                    $ongoing3=$rows['ongoing3'];

                    $completed1=$rows['completed1'];
                    $completed2=$rows['completed2'];
                    $completed3=$rows['completed3'];

                    $news1=$rows['news1'];
                    $news2=$rows['news2'];
                    $news3=$rows['news3'];
                    

                    $status=$rows["status"];
                }

            ?>
<div id="id01">
    <div class="modal-content">
        <div class="w3-container w3-padding">

            <div class="row">
      <div class="col-xs-7">
      <div class="thumbnail">
               <?php    
                $uploads_dir = 'faculty_data/';
               $imagename = $uploads_dir.$userid . '.png';

           if(file_exists("$imagename"))
           {
           echo " <img id='profile' width='210px' height=' 210px' src='".$imagename."'>";
       } 
       else
            {
                echo " <img  id='profile' width='200px' height=' 230px' src='no_image.png'>";
            }

       ?>
          
          <div class="caption">
            <p><span class="w3-wide nav-heading  w3-text-grey" style="font-size:17px;font-weight:900;border-radius:5px;">
                <?php echo strtoupper($name);
               // $uid=$_SESSION["name"];
                ?>
             </span> 
          </p>
          </div>
      </div>
      
    </div>
    <div class="col-xs-5" style="text-align:right;">
       <a href="faculty.php"><span class="w3-wide nav-heading  w3-text-grey" style="font-size:17px;font-weight:900;border-radius:5px;text-decoration:underline;"> Previous page</span></a>     </div><br><br>
        
           
            <table class="table table-striped table-hover table-modal-disp">
               
                <tr id="Academic_Appointment_e_show">
                    <th>Academic Appointment :</th>
                    <td id="Academic_Appointment_e"><?=$appointment?></td>
                </tr>
                <tr id="Additional_Appointment_e_show">
                    <th>Additional Appointment :</th>
                    <td id="Additional_Appointment_e"><?=$addappointment?></td>
                </tr>
                <tr>
                    <th>Academic Department :</th>
                    <td id="Academic_Department_e"><?=$academicdep?></td>
                </tr>
                <tr>
                    <th>Contact Address :</th>
                    <td id="Contact_Address_e"><?=$address1.' '.$address2.' '.$address3.' '.$address4?></td>
                </tr>
                <tr>
                    <th>Email :</th>
                    <td id="Email_e"><?=$email?></td>
                </tr>
                <tr>
                    <th>Phone :</th>
                    <td id="Phone_e"><?=$phone?></td>
                </tr>

                <tr id="Awards_e_show">
                    <th>Awards and Honour :</th>
                    <td>
                        <span class="disp_new_line" id="Awards_ee1"><?=$award1?></span>
                        <span class="disp_new_line"id="Awards_ee2"><?=$award2?></span>
                        <span class="disp_new_line"id="Awards_ee3"><?=$award3?></span>
                        <span class="disp_new_line"id="Awards_ee4"><?=$award4?></span>
                        <span class="disp_new_line"id="Awards_ee5"><?=$award5?></span>
                    </td> 

                     
                </tr>
                <tr>
                    <th>Educational Qualifications :</th>
                    <td id="Education_e_show">
                     <span id="Education_e1" class="disp_new_line">   <?=$qualification1?> </span>
                     <span id="Education_e2" class="disp_new_line">   <?=$qualification2?> </span>
                     <span id="Education_e3" class="disp_new_line">   <?=$qualification3?> </span>
                     <span id="Education_e4" class="disp_new_line">   <?=$qualification4?> </span>
                     <span id="Education_e5" class="disp_new_line">   <?=$qualification5?> </span>
                    </td>
                </tr>
                <tr>
                    <th>Research Interests Details :</th>
                    <td id="Research_e"> 
                    <span class="disp_new_line"> <?=$research1?> </span> 
                    <span class="disp_new_line"> <?=$research2?> </span> 
                    <span class="disp_new_line"> <?=$research3?> </span> 
                    </td>
                </tr>
                <tr id="Membership_e_show">
                    <th>Membership of Professional Bodies :</th>
                    <td id="Membership_e"> <?=$membership?> </td>
                </tr>
                <tr id="Highlighted_e_show">
                    <th>Highlighted Publications :</th>
                    <td id="Highlighted_e">
                        <span class="disp_new_line" id="Highlighted_e1" >  <?=$publication1?> </span>
                        <span class="disp_new_line" id="Highlighted_e2">  <?=$publication2?>  </span>
                        <span class="disp_new_line" id="Highlighted_e3">  <?=$publication3?> </span>
                        <span class="disp_new_line" id="Highlighted_e4"> <?=$publication4?> </span>
                        <span class="disp_new_line" id="Highlighted_e5"><?=$publication5?>  </span>
                    </td>
                </tr>
                <tr id="Ongoing_e_show">
                    <th>Ongoing Projects :</th>
                    <td id="Ongoing_e">
                        <span class="disp_new_line" id="Ongoing_e1"> <?=$ongoing1?>  </span>
                        <span class="disp_new_line" id="Ongoing_e2"> <?=$ongoing2?> </span>
                        <span class="disp_new_line"id="Ongoing_e3"> <?=$ongoing3?>  </span>
                    </td>
                </tr>
                <tr id="completed_e_show">
                    <th>Completed Projects :</th>
                    <td id="completed_e">
                        <span class="disp_new_line" id="Completed_e1"> <?=$completed1?>  </span>
                        <span class="disp_new_line" id="Completed_e2"> <?=$completed2?> </span>
                        <span class="disp_new_line" id="Completed_e3"> <?=$completed3?> </span>
                    </td>
                </tr>
                <tr id="News_e_show">
                    <th>In the news :</th>
                    <td id="News_e">
                        <span class="disp_new_line" id="news_e1"> <?=$news1?>    </span>
                        <span class="disp_new_line" id="news_e2"> <?=$news2?>  </span>
                        <span class="disp_new_line" id="news_e3"> <?=$news3?>   </span>
                    </td>

                </tr>
                <?php
                $uploads_dir = 'faculty_data/';
                 $cvname = $uploads_dir.$userid . '.pdf';
                if(file_exists("$cvname"))
                {   ?>
                <tr>
                <th> Link to CV: </th>
                <td> <a href="<?=$cvname?>" style="color:rgb(139,0,0);text-decoration:underline;font-variant:small-caps;font-size:18px;font-weight:bold;"> Click Here to open your CV</a></td>

                </tr>
                <?php } ?>


            </table> <br>
        
</div>
</div>
</div>
</div>
</body>
<script >

var ba = document.getElementById('Additional_Appointment_e').innerText;

console.log(ba);
if( ba == '')
{
   document.getElementById('Additional_Appointment_e_show').style.display = 'none'; 
}
else
    {console.log("nmcs"); }

var ab1 = document.getElementById('Awards_ee1').innerText;
var ab2 = document.getElementById('Awards_ee2').innerText;
var ab3 = document.getElementById('Awards_ee3').innerText;
var ab4 = document.getElementById('Awards_ee4').innerText;
var ab5 = document.getElementById('Awards_ee5').innerText;
console.log(ab1);console.log(ab2);console.log(ab3);console.log(ab4);console.log(ab5);
if( ab1 == '' && ab2 == '' && ab3 == '' && ab4 == '' && ab5 == '')
   {
    document.getElementById('Awards_e_show').style.display = 'none';
}
else
    {console.log("nmcs"); }

var ab1 = document.getElementById('Membership_e').innerText;
if( ab1 == '')
{
    document.getElementById('Membership_e_show').style.display = 'none';
}
else
{
    console.log("nmcs"); ;
}

var ab1 = document.getElementById('Highlighted_e1').innerText;
var ab2 = document.getElementById('Highlighted_e2').innerText;
var ab3 = document.getElementById('Highlighted_e3').innerText;
var ab4 = document.getElementById('Highlighted_e4').innerText;
var ab5 = document.getElementById('Highlighted_e5').innerText;

if( ab1 == '' && ab2 == '' && ab3 == '' && ab4 == '' && ab5 == '')
   {
    document.getElementById('Highlighted_e_show').style.visibility = 'hidden';
}
else
    {console.log('mncs');}  

var ab1 = document.getElementById('Ongoing_e1').innerText;
var ab2 = document.getElementById('Ongoing_e2').innerText;
var ab3 = document.getElementById('Ongoing_e3').innerText;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('Ongoing_e_show').style.display = 'none';
}
else
    {console.log('mncs');}

var ab1 = document.getElementById('Completed_e1').innerText;
var ab2 = document.getElementById('Completed_e2').innerText;
var ab3 = document.getElementById('Completed_e3').innerText;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('completed_e_show').style.display = 'none';
}
else
    {console.log('mncs');}

var ab1 = document.getElementById('news_e1').innerText;
var ab2 = document.getElementById('news_e2').innerText;
var ab3 = document.getElementById('news_e3').innerText;

if( ab1 == '' && ab2 == '' && ab3 == '')
{
    document.getElementById('News_e_show').style.display = 'none';
}
else
    {console.log('mncs');} 
</script>
</html>