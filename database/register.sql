-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 07:57 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `userid` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `name` varchar(100) NOT NULL,
  `appointment` varchar(1000) NOT NULL,
  `addappointment` varchar(1000) NOT NULL,
  `academicdep` varchar(1000) NOT NULL,
  `address1` varchar(1000) NOT NULL,
  `address2` varchar(1000) NOT NULL,
  `address3` varchar(1000) NOT NULL,
  `address4` varchar(1000) NOT NULL,
  `emails` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `award1` varchar(1000) NOT NULL,
  `award2` varchar(255) NOT NULL,
  `award3` varchar(255) NOT NULL,
  `award4` varchar(255) NOT NULL,
  `award5` varchar(255) NOT NULL,
  `qualification1` varchar(1000) NOT NULL,
  `qualification2` varchar(256) NOT NULL,
  `qualification3` varchar(256) NOT NULL,
  `qualification4` varchar(256) NOT NULL,
  `qualification5` varchar(256) NOT NULL,
  `research1` varchar(1000) NOT NULL,
  `research2` varchar(356) NOT NULL,
  `research3` varchar(356) NOT NULL,
  `interest` varchar(1000) NOT NULL,
  `membership` varchar(1000) NOT NULL,
  `publication1` varchar(1000) NOT NULL,
  `publication2` varchar(10000) NOT NULL,
  `publication3` varchar(10000) NOT NULL,
  `publication4` varchar(10000) NOT NULL,
  `publication5` varchar(10000) NOT NULL,
  `ongoing1` varchar(1000) NOT NULL,
  `ongoing2` varchar(1000) NOT NULL,
  `ongoing3` varchar(1000) NOT NULL,
  `completed1` varchar(1000) NOT NULL,
  `completed2` varchar(1000) NOT NULL,
  `completed3` varchar(1000) NOT NULL,
  `news1` varchar(256) NOT NULL,
  `news2` varchar(256) NOT NULL,
  `news3` varchar(256) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `userid`, `password`, `name`, `appointment`, `addappointment`, `academicdep`, `address1`, `address2`, `address3`, `address4`, `emails`, `phone`, `award1`, `award2`, `award3`, `award4`, `award5`, `qualification1`, `qualification2`, `qualification3`, `qualification4`, `qualification5`, `research1`, `research2`, `research3`, `interest`, `membership`, `publication1`, `publication2`, `publication3`, `publication4`, `publication5`, `ongoing1`, `ongoing2`, `ongoing3`, `completed1`, `completed2`, `completed3`, `news1`, `news2`, `news3`, `status`) VALUES
(1, 'admin@admin.com', 'admin', 'Vedant Sharma', 'Student', '', 'Department of CSE', '69', 'gandhi puram', 'izzatnagar', 'Bareilly', 'vedant.sharma72@gmail.com', '9557587232', '', 'participation', '', '', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', '', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 'Contrary to popular belief, Lorem Ipsum is not simply random text', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text', 1),
(3, 'rav_kat@cse', 'rav_kat@cse95426', 'Er. Ravindra Katiyar', '', '', 'Computer Science and engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(4, 'ren_jai@cse', 'ren_jai@cse34766', 'Dr. Renu jain', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(5, 'dee_kum@cse', 'dee_kum@cse62922', 'Er. Deepak Kumar Verma       ', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(6, 'san_gup@cse', 'san_gup@cse33557', 'Dr. Sandesh Gupta', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(7, 'alo_kum@cse', 'alo_kum@cse76289', 'Dr. Alok Kumar', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(8, 'she_tiw@cse', 'she_tiw@cse78215', 'Er. Shesh Mani Tiwari', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(9, 'sha_ala@cse', 'sha_ala@cse25740', 'Er. Mohd. Shah Alam', '', '', 'Computer Science and Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(10, 'pus_mam@ca', 'pus_mam@ca00361', 'Mrs. Pushpa Mamoria', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(11, 'ani_yad@ca', 'ani_yad@ca01832', 'Dr. Anil Yadav', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(12, 'mam_tiw@ca', 'mam_tiw@ca43129', 'Mr. Mamta Tiwari', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(13, 'omk_aga@ca', 'omk_agh@ca42894', 'Mr.Omkar Agarhari', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(14, 'mam_pan@ca', 'mam_pan@ca23561', 'Mrs. Mamta Mayee Panda      ', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(15, 'ami_vir@ca', 'ami_vir@ca98311', 'Mr. Amit Virmani', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(16, 'pra_sri@ca', 'pra_sri@ca55668', 'Mr. Prashant Srivastava', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(17, 'abh_dwi@ca', 'abh_dwi@ca00113', 'Mr. Abhishek Dwivedi', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(18, 'him_shu@ca', 'him_shu@ca89220', 'Mr. Himanshu Shukla', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(19, 'ami_sax@ca', 'ami_sax@ca55280', 'Mr. Amit Saxena', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(20, 'akh_sin@ca', 'akh_sin@ca92318', 'Mr. Akhlish Singh', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(21, 'she_ver@ca', 'she_ver@ca78993', 'Mr. Shekhar Verma', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(22, 'may_rah@ca', 'may_rah@ca77119', 'Mr. Mayur Rahul', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(23, 'shi_dub@ca', 'shi_dub@ca01910', 'Mr. Shilpi Dubey', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(24, 'shi_tri@ca', 'shi_tri@ca12353', 'Mr. Shivneet Tripathi', '', '', 'Computer Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(25, 'nir_kum@ece', 'nir_kum@ece91932', 'Er. Niraj kumar', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(26, 'ric_ver@ece', 'ric_ver@ece37271', 'Er. Richa Verma', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(27, 'par_awa@ece', 'par_awa@ece23241', 'Er. Parul Awasthi', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(28, 'vis_awa@ece', 'vis_awa@ece88236', 'Er. Vishal Awasthi', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(29, 'aje_sri@ece', 'aje_sri@ece81991', 'Er. Ajeet Kr. Srivastava', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(31, 'ana_gup@ece', 'ana_gup@ece72872', 'Er. Anand Gupta', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(32, 'pra_bis@ece', 'pra_bis@ece11421', 'Er. Prashant Bisht', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(33, 'aja_tiw@ece', 'aja_tiw@ece91018', 'Er. Ajay Tiwari', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(34, 'ami_kat@ece', 'ami_kat@ece43442', 'Er. Amit Katiyar', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(35, 'som_mal@ece', 'som_mal@ece72199', 'Er. Somesh Malhotra', '', '', 'Electronics & Communication Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(36, 'ram_nir@me', 'ram_nir@me44883', 'Er. Ramendra Singh Niranjan  ', '', '', 'Mechanical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(37, 'arp_sri@me', 'arp_sri@me71897', 'Er. Arpit Kumar Srivastava  ', '', '', 'Mechanical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(38, 'sha_kum@me', 'sha_kum@me81276', 'Er. Shatrujeet Kumar Mall    ', '', '', 'Mechanical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(39, 'pra_sin@me', 'pra_sin@me01893', 'Er. Pramod Kumar Singh', '', '', 'Mechanical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(40, 'bri_mit@ce', 'bri_mit@ce91891', 'Dr. Brishti Mitra', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(41, 'vin_sac@ce', 'vin_sac@ce81933', 'Er. Vinay Kr. Sachan', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(42, 'pra_kum@ce', 'pra_kum@ce81983', 'Er. Pradeep Kumar', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(43, 'aru_gup@ce', 'aru_gup@ce61771', 'Er. Arun Kr. Gupta', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(44, 'abh_cha@ce', 'abh_cha@ce13142', 'Er. Abhishek Chandra', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(45, 'ume_sha@ce', 'ume_sha@ce79824', 'Er. Umesh Chandra Sharma ', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(46, 'pra_pat@ce', 'pra_pat@ce81083', 'Er. Praveen Bhai Patel', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(47, 'arv_sen@ce', 'arv_sen@ce68229', 'Er. Arvind Sengar', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(48, 'vai_sax@ce', 'vai_sax@ce12871', 'Er. Vaibhav Saxena', '', '', 'Chemical Engineering', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(49, 'vij_kas@msme', 'vij_kas@msme33422', 'Mr. Vijay Kumar Kashyap', '', '', 'MSME', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(50, 'bhu_yad@msme', 'bhu_yad@msme45262', 'Ms. Bhumika Yadav', '', '', 'MSME', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(51, 'mam_sag@bio', 'mam_sag@bio72189', 'Mrs. Mamta Sagar', '', '', 'Bioinformatics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(52, 'man_gup@bio', 'man_gup@bio76222', 'Mr. Manish Kr. Gupta', '', '', 'Bioinformatics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(53, 'bri_sin@bio', 'bri_sin@bio77762', 'Mr. Brijendra Singh', '', '', 'Bioinformatics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(54, 'ras_aga@it', 'ras_aga@it55510', 'Dr. Rashi Agarwal', '', '', 'Information Technology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(55, 'ram_mis@it', 'ram_mis@it56292', 'Mr. Ram Narayan MIshra', '', '', 'Information Technology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(56, 'pre_cha@it', 'pre_cha@it23245', 'Ms. Preeti Chaturvedi', '', '', 'Information Technology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(57, 'san_kum@it', 'san_kum@it79281', 'Mr. Sanjeet Kumar', '', '', 'Information Technology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(58, 'ami_xyz@it', 'ami_xyz@it54300', 'Mr. Amitesh', '', '', 'Information Technology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(59, 'arp_yad@che', 'arp_yad@che58892', 'Dr. Arpita Yadav', '', '', 'Chemistry', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(60, 'bir_sin@che', 'bir_sin@che66621', 'Dr. Birendra Pratap Singh       ', '', '', 'Chemistry', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(61, 'ras_dub@che', 'ras_dub@che66620', 'Dr. Rashmi Dubey', '', '', 'Chemistry', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(62, 'rat_shu@che', 'rat_shu@che88281', 'Dr. Ratna Shukla', '', '', 'Chemistry', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(63, 'vn_pal@math', 'vn_pal@math43500', 'Dr. V.N Pal', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(64, 'var_gup@math', 'var_gup@math22354', 'Dr. Varsha Gupta', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(65, 'sau_por@math', 'sau_por@math56261', 'Dr. Saurabh Porwal', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(66, 'man_sin@math', 'man_sin@math68210', 'Dr. Monoj Singh', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(67, 'dk_sin@math', 'dk_sin@math90101', 'Dr. D.K Singh', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(68, 'poo_dix@math', 'poo_dix@math33301', 'Dr. Poonam Dixit', '', '', 'Mathematics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(69, 'sas_sar@phy', 'sas_sar@phy77711', 'Dr. Saswati Sarkar', '', '', 'Department of Physics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(70, 'anj_dix@phy', 'anj_dix@phy87653', 'Dr. Anju Dixit', '', '', 'Department of Physics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(71, 'ram_jan@phy', 'ram_jan@phy91010', 'Dr. Ram Janma', '', '', 'Department of Physics', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(72, 'niy_pad@hss', 'niy_pad@hss74822', 'Dr. Niyati Padhi', '', '', 'Humanities & Social Science', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(73, 'ric_ver@hss', 'ric_ver@hss88818', 'Dr. Richa Verma', '', '', 'Humanities & Social Science', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(74, 'vin_kur@hss', 'vin_kur@hss13291', 'Dr. Vinita Kurshresth', '', '', 'Humanities & Social Science', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(75, 'a_basu@hss', 'a_basu@hss12237', 'Wg. Cdr. A. Basu', '', '', 'Humanities & Social Science', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(76, 'shi_kai@biotech', 'shi_kai@biotech99832', 'Dr. Shilpa Kaistha', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(77, 'jhu_dat@biotech', 'jhu_dat@biotech66432', 'Dr. Jhuma Datta', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(78, 'sha_kat@biotech', 'sha_kat@biotech44890', 'Dr. Shashwat Katiyar', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(79, 'man_tri@biotech', 'man_tri@biotech44628', 'Dr. Manishi Tripathi', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(80, 'mad_sin@biotech', 'mad_sin@biotech44801', 'Dr. Madhulika Singh', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(81, 'anj_ver@biotech', 'anj_ver@biotech44231', 'Dr. Anju Verma', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(82, 'var_gup@biotech', 'var_gup@biotech44881', 'Dr. Varsha Gupta', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(83, 'vis_cha@biotech', 'vis_cha@biotech44001', 'Mr. Vishal Chand', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(84, 'dha_sin@biotech', 'dha_sin@biotech22441', 'Dr. Dharam Singh', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(85, 'shh_utt@biotech', 'shh_utt@biotech22998', 'Mrs. Shhilpi Uttam', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(86, 'arc_yad@biotech', 'arc_yad@biotech77551', 'Dr. Archana Yadav', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(87, 'ekt_kha@biotech', 'ekt_kha@biotech22340', 'Ms. Ekta Khare', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(88, 'swa_sri@biotech', 'swa_sri@biotech72943', 'Dr. Swati Srivastava', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(89, 'ann_sin@biotech', 'ann_sin@biotech55320', 'Dr. Annika Singh', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(90, 'ana_dix@biotech', 'ana_dix@biotech65335', 'Dr. Anamika Dixit', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(91, 'sha_yad@biotech', 'sha_yad@biotech24232', 'Ms. Shalini Verma', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(92, 'aam_zai@biotech', 'aam_zai@biotech22446', 'Ms. Aamena Zaidi', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(93, 'buk_sin@biotech', 'buk_sin@biotech90905', 'Dev Buksh Singh', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(94, 'vij_sin@biotech', 'vij_sin@biotech65830', 'Dr. Vijay Kumar Singh', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(95, 'nee_sri@biotech', 'nee_sri@biotech78292', 'Dr. Neerja Srivastava', '', '', 'Institute of BioSciences and BioTechnology', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(96, 'sk_sri@ibm', 'sk_sri@ibm74922', 'PROF. S.K. SRIVASTAVA', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(97, 'rc_kat@ibm', 'rc_kat@ibm89101', 'PROF. R.C. KATIYAR', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(98, 'muk_ran@ibm', 'muk_ran@ibm99221', 'PROF. MUKESH RANGA', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(99, 'sud_pan@ibm', 'sud_pan@ibm87292', 'DR. SUDHANSHU PANDIYA', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(100, 'ans_yad@ibm', 'ans_yad@ibm29384', 'DR. ANSHU YADAV', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(101, 'nee_sin@ibm', 'nee_sin@ibm23402', 'DR. NEERAJ KR. SINGH', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(102, 'sud_sri@ibm', 'sud_sri@ibm21021', 'MR. SUDESH KR. SRIVASTAVA  ', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(103, 'mri_sin@ibm', 'mri_sin@ibm00021', 'DR. MRIDULESH SINGH', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(104, 'sid_rai@ibm', 'sid_rai@ibm02304', 'MR. SIDHANSHU RAI', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(105, 'sud_ver@ibm', 'sud_ver@ibm92092', 'MR. SUDHIR VERMA', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(106, 'cha_kha@ibm', 'cha_kha@ibm93942', 'MRS. CHARU KHAN', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(107, 'viv_sac@ibm', 'viv_sac@ibm23982', 'DR. VIVEK SINGH SACHAN', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(108, 'war_sin@ibm', 'war_sin@ibm90009', 'MRS. WARSHI SINGH', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(109, 'apa_kat@ibm', 'apa_kat@ibm23092', 'MRS. APARNA KATIYAR', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(110, 'akh_dix@ibm', 'akh_dix@ibm98880', 'DR. AKHILESH DIXIT', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(111, 'sac_sha@ibm', 'sac_sha@ibm02020', 'MR. SACHIN SHARMA', '', '', 'Institute of Business Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(112, 'raj_kum@lsd', 'raj_kum@lsd13234', 'Dr.Rajesh Kumar', '', '', 'Life Science Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(113, 'nan_yad@lsd', 'nan_yad@lsd82372', 'Dr. Nand Lal Yadav', '', '', 'Life Science Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(114, 'sk_awa@lsd', 'sk_awa@lsd78965', 'Dr. S.K.Awasthi', '', '', 'Life Science Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(115, 'ash_yad@lis', 'ash_yad@lis79754', 'Dr. Anshu Yadav', '', '', 'Dep. Of Library and Information Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(116, 'ruc_kri@lis', 'ruc_kri@lis45646', 'Dr. Ruchika Krishna ', '', '', 'Dep. Of Library and Information Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(117, 'smi_gup@lis', 'smi_gup@lis92429', 'Dr. Smita Gupta', '', '', 'Dep. Of Library and Information Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(118, 'dee_nig@lis', 'dee_nig@lis781249', 'Dr. Deepmala Nigam', '', '', 'Dep. Of Library and Information Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(119, 'pra_kat@ips', 'pra_kat@ipsy23921', 'Dr. Praveen Katiyar', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(120, 'ver_pra@ips', 'ver_pra@ips91024', 'Dr. Versha Prasad', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(121, 'mun_ras@ips', 'mun_ras@ips90129', 'Dr. Munish Rastogi', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(122, 'cha_kum@ips', 'cha_kum@ips28092', 'Mr. Chandrasekhar Kumar', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(123, 'dig_sha@ips', 'dig_sha@ips21309', 'Mr. Digvijay Sharma', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(124, 'neh_shu@ips', 'neh_shu@ips90190', 'Mrs. Neha Shukla', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(125, 'kau_pan@ips', 'kau_pan@ips13209', 'Dr. Kaushlendra Kumar Pandey', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(126, 'sha_roh@ips', 'sha_roh@ips80243', 'Dr. Shalini Rohtagi', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(127, 'vir_nig@ips', 'vir_nig@ips82340', 'Dr. Virendra Nigam', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(128, 'bha_dix@ips', 'bha_dix@ips82300', 'Dr. Bharti Dixit', '', '', 'Institute of Paramedical Sciences', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(129, 'arv_sin@ijmc', 'arv_sin@ijmc39221', 'Dr. Arvind Kumar Singh', '', '', 'Institute of Journalism and Mass Communication', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(130, 'jit_dob@ijmc', 'jit_dob@ijmc39090', 'Mr. Jitendra Dobral', '', '', 'Institute of Journalism and Mass Communication', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(131, 'ras_gau@ijmc', 'ras_gau@ijmc38291', 'Mrs. Rashmi Gautam', '', '', 'Institute of Journalism and Mass Communication', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(132, 'aja_sin@dsw', 'aja_sin@dsw23210', 'Dr. Ajay Pratap Singh', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(133, 'ran_ver@dsw', 'ran_ver@dsw58880', 'Dr. Ranjeev Verma', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(134, 'kir_jha@dsw', 'kir_jha@dsw39022', 'Dr. Kiran Jha ', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(135, 'ani_awa_dsw', 'ani_awa_dsw29029', 'Dr. Anita Awasthi', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(136, 'sat_ver@dsw', 'sat_ver@dsw84333', 'Dr. Satya Prakash Verma', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(137, 'urvashi@dsw', 'urvashi@dsw83921', 'Mrs. Urvashi', '', '', 'Department of Social Work', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(138, 'nis_sha@ip', 'nis_sha@ip28439', 'Mrs. Nisha Sharma', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(139, 'aja_gup@ip', 'aja_gup@ip72392', 'Mr. Ajay Kumar Gupta  ', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(140, 'a_raj@ip', 'a_raj@ip82111', 'Mr. A. Rajendiran', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(141, 'mee_gup@ip', 'mee_gup@ip23893', 'Mrs. Meenakshi Gupta', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(142, 'nik_sac@ip', 'nik_sac@ip47293', 'Mr. Nikhil Kr. Sachan   ', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(143, 'kal_kus@ip', 'kal_kus@ip32700', 'Mrs. Kalpana Kushwaha', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(144, 'pra_gup@ip', 'pra_gup@ip45933', 'Mr. Prakash Chandra Gupta', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(145, 'vin_doh@ip', 'vin_doh@ip34000', 'Mr. Vinod Doharey', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(146, 'see_pus@ip', 'see_pus@ip37489', 'Mrs. Seema Pushkar', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(147, 'anj_sin@ip', 'anj_sin@ip21221', 'Mrs. Anju Singh', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(148, 'pra_kat@ip', 'pra_kat@ip24221', 'Mrs. Pratima Katiyar', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(149, 'aja_kum@ip', 'aja_kum@ip87483', 'Mr. Ajay Kumar', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(150, 'sha_mis@ip', 'sha_mis@ip22282', 'ShashtKiran Mishra', '', '', 'Institute of Pharmacy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(151, 'sub_aga@edu', 'sub_aga@edu77383', 'Dr. Subhash Chandra Agarwal', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(152, 'mun_kum@edu', 'mun_kum@edu74322', 'Dr. Munesh Kumar', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(153, 'gau_rao@edu', 'gau_rao@edu73783', 'Dr. Gaurav Rao', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(154, 'ras_gor@edu', 'ras_gor@edu64323', 'Dr. Rashmi Gore', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(155, 'kanchan@edu', 'kanchan@edu23871', 'Mrs. Kanchan', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(156, 'jai_raj@edu', 'jai_raj@edu23792', 'Dr. Jai Shree Rajput', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(157, 'kal_agn@edu', 'kal_agn@edu11242', 'Dr. Kalpna Agnihotri', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(158, 'anu_yad@edu', 'anu_yad@edu22881', 'Mrs. Anupma Yadav', '', '', 'Education Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(159, 'san_swa@eng', 'san_swa@eng22101', 'Dr. Sanjay Swarnkar', '', '', 'English Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(160, 'pk_kus@eng', 'pk_kus@eng11124', 'Dr. P. K. Kush', '', '', 'English Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(161, 'sum_bis@eng', 'sum_bis@eng20481', 'Dr. Suman Biswas', '', '', 'English Department', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(162, 'san_sin@dace', 'san_sin@dace11322', 'Dr. Sandeep Kumar Singh', '', '', 'Department of Adult and Continuing Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(163, 'upm_sri@dace', 'upm_sri@dace13839', 'Dr. Upma Srivastava', '', '', 'Department of Adult and Continuing Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(164, 'shi_bha@dace', 'shi_bha@dace24789', 'Dr. Shishupal Singh Bhadouria', '', '', 'Department of Adult and Continuing Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(165, 'raj_sin@dpe', 'raj_sin@dpe72982', 'Dr. Rajesh Pratap Singh', '', '', 'Department of Physical Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(166, 'upe_pan@dpe', 'upe_pan@dpe72800', 'Sri Upendra Pandey', '', '', 'Department of Physical Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(167, 'viv_sin@dpe', 'viv_sin@dpe71249', 'Dr. Vivek Kumar Singh', '', '', 'Department of Physical Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(168, 'dha_sin@dpe', 'dha_sin@dpe27938', 'Sri Dhananjay Singh', '', '', 'Department of Physical Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(169, 'gau_sha@dpe', 'gau_sha@dpe29735', 'Sri Gaurav Sharma', '', '', 'Department of Physical Education', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(170, 'rag_swa@dm', 'rag_swa@dm83529', 'Dr. Ragini Swarnkar', '', '', 'Department of Music', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(171, 'bra_kat@ifa', 'bra_kat@ifa72984', 'Dr. Brajesh Swaroop Katiyar', '', '', 'Institute of Fine Arts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(172, 'pra_sin@ifa', 'pra_sin@ifa79823', 'Dr. Prahlad Singh ', '', '', 'Institute of Fine Arts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(173, 'ans_mis@ifa', 'ans_mis@ifa29482', 'Smt. Anshuma Mishra', '', '', 'Institute of Fine Arts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(174, 'vis_yad@ifa', 'vis_yad@ifa32236', 'Vishal Yadav', '', '', 'Institute of Fine Arts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(175, 'jiu_bal@ifa', 'jiu_bal@ifa74292', 'Jiute Bali', '', '', 'Institute of Fine Arts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(176, 'viv_sac@ihtm', 'viv_sac@ihtm72935', 'Dr. Vivek Singh Sachan', '', '', 'Institute of Hotel & Tourism Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(177, 'sau_tri@ihtm', 'sau_tri@ihtm21749', 'Mr. Saurabh Tripathi', '', '', 'Institute of Hotel & Tourism Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(178, 'anu_agn@ihtm', 'anu_agn@ihtm72932', 'Mr. Anuragh Agnihotri', '', '', 'Institute of Hotel & Tourism Management', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
